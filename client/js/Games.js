const Home = window.httpVueLoader('./components/Home.vue')
const Game1 = window.httpVueLoader('./components/Game1.vue')
const Game2 = window.httpVueLoader('./components/Game2.vue')
const Test = window.httpVueLoader('./components/Test.vue')

const routes = [
  { path: '/', component: Home },
  { path: '/Game1', component: Game1 },
  { path: '/Game2', component: Game2 },
  { path: '/Test', component: Test }
]

const router = new VueRouter({
  routes
})

var app = new Vue({
  router,
  el: '#app',
  data: {
  },
  async mounted () {
  },
  methods: {
  }
})
