const Game1 = window.httpVueLoader('./components/Game1.vue')
const Game2 = window.httpVueLoader('./components/Game2.vue')
const Home = window.httpVueLoader('./components/Home.vue')

const routes = [
  { path: '/game1', component: Game1 }, 
  { path: '/game2', component: Game2 }, 
  { path: '/home', component: Home }, 
]

const router = new VueRouter({
  routes
})

var app = new Vue({
  router,
  el: '#app',
})
