const express = require('express')
const router = express.Router()

const bcrypt = require('bcrypt')
const { Client } = require('pg')
const client = new Client({
  user: 'postgres',
  host: 'localhost',
  password: 'postgrespwd',
  database: 'EazOrga'
})

client.connect()

/* ----------------------- BEGIN of Login / Register / Logout / Me APIs --------------------------------- */

router.post('/register', async (req, res) => { // -> register API
  const username = req.body.username
  const email = req.body.email
  const password = req.body.password

  if (typeof username !== 'string' || username === '' ||typeof email !== 'string' || email === '' || // input verifications
      typeof password !== 'string' || password === '') {
    res.status(400).json({ message: 'bad request' })
    return
  }

  const query = await client.query({
    text: "SELECT * FROM users WHERE email = $1 LIMIT 1",  // we check the email in our database users
    values: [email]
  })

  if (query.rows.length !== 0) { // if the mail is already taken, we return an error message
    res.status(400).json({ message: 'email taken' }) 
    return
  }

  const hash = await bcrypt.hash(password, 10) // we cypher the  password of the user before sending it in our database

  await client.query({
    text: "INSERT INTO users (username, email, password) VALUES ($1, $2, $3)", // we insert the mail and the cyphered password 
    values: [email, hash]
  })

  res.send() // we send the inserted information to our "users" database
})

router.post('/login', async (req, res) => { // -> login API
  if (req.session.userId !== undefined) {
    res.status(401).json({ message: 'already logged in' })
    return
  }

  const username = req.body.username
  const email = req.body.email
  const password = req.body.password

  if (typeof username !== 'string' || username === '' || typeof email !== 'string' || email === '' || // input verifications
      typeof password !== 'string' || password === '') {
    res.status(400).json({ message: 'bad request' })
    return
  }

  const query = await client.query({
    text: "SELECT * FROM users WHERE email = $1 LIMIT 1", // we verify in our "users" database if the email exist
    values: [email]
  })

  if (query.rows.length === 0) { // if not, we return an error message
    res.status(400).json({ message: 'incorrect credentials' })
    return
  }

  const user = query.rows[0]
  const valid = await bcrypt.compare(password, user.password) // we verify if the input password is the same as the one in the database

  if (!valid) {
    res.status(400).json({ message: 'incorrect credentials' })
    return
  }

  req.session.userId = user.id

  res.send()
})

router.post('/logout', async (req, res) => { // -> logout API
  if (req.session.userId === undefined) {
    res.status(401).json({ message: 'not logged in' })
    return
  }

  delete req.session.userId // we supress the user session

  res.send()
})

router.get('/me', async (req, res) => { // -> me API
  if (req.session.userId === undefined) {
    res.status(401).json({ message: 'not logged in' })
    return
  }

  const query = await client.query({
    text: "SELECT * FROM users WHERE id = $1 LIMIT 1",
    values: [req.session.userId]
  })

  if (query.rows.length === 0) {
    res.status(400).json({ message: 'user no longer existent' })
    return
  }

  const user = query.rows[0]

  res.json({
    id: user.id,
    username: user.username,
    email: user.email
  })
})

/* ----------------------- END of Login / Register / Logout / Me APIs --------------------------------- */

module.exports = router
